var myNamespace = {};

(function ($) {
    "use strict";

    myNamespace.MyViewModel = function (id, name, data) {
        this.id = id;
        this.name = name;
        this.data = data;

        this.showElement = function (element) {
            $.show(element);
        }
    };

    // Export to Node.js
    if (typeof module === "object") {
        module.exports = myNamespace;
    }
})(jQuery);