var gulp = require("gulp");
var jasmine = require("gulp-jasmine");

var files = [
    "tests/globals.js",
    "tests/**/*.tests.js"
];

gulp.task("test", function() {
    return gulp.src(files)
        .pipe(jasmine({
            verbose: true
        }));
});

gulp.task('cover', function(cb) {
    var istanbul = require('gulp-istanbul');

    gulp.src(['src/**/*.js'])
        .pipe(istanbul({ includeUntested: true })) // Covering all files
            .pipe(istanbul.hookRequire()) // Force `require` to return covered files
            .on('finish', function() {
                gulp.src(files)
                    .pipe(jasmine())
                    .pipe(istanbul.writeReports()) // Creating the reports after tests runned
                    .pipe(istanbul.enforceThresholds({ thresholds: { global: 90 } })) // Enforce a coverage of at least 90%
                    .on('end', cb);
            });
});

gulp.task("default", ["test"]);