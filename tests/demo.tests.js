(function () {
    "use strict";

    var myNamespace = require("../src/code.js");

    describe("A suite", function () {

        it("contains spec with an expectation", function () {
            expect(true).toBe(true);
        });

        it("should fail", function () {
            expect(true).toBe(false); // Need to be fixed for 'cover' to work.
        });

        describe("Jasmine showcase", function () {
            it("toBe", function () {
                expect(true).toBe(true);
            });

            it("not", function () {
                expect(false).not.toBe(true);
            });

            it("toBe null", function () {
                var a = null;
                expect(a).toBe(null);
            });

            it("toBeGreaterThan", function () {
                expect(42).toBeGreaterThan(20);
            });

            it("toEqual", function () {
                var a = 42;
                expect(a).toEqual(42);
            });

            it("toEqual with objects", function () {
                var a = { name: "Luke", alliance: "Light" };
                var b = { alliance: "Light", name: "Luke" };
                expect(a).toEqual(b);
            });

            it("toMatch with regular expression", function () {
                expect("Luke Skywalker").toMatch("S.+w");
            });

            it("toBeDefined", function () {
                var luke = { name: "Luke", alliance: "Light" };
                expect(luke.presentInEpisodeVII).not.toBeDefined();
            });

            it("toContain", function () {
                var a = ["Luke", "Leia", "Anakin"];
                expect(a).not.toContain("Jar Jar");
            });
        });

        describe("Setup and Teardown", function () {
            beforeEach(function () { });
            afterEach(function () { });

            beforeAll(function () { });
            afterAll(function () { });
        });

        describe("For code.js", function () {
            it("with a shim", function () {
                var vm = new myNamespace.MyViewModel(42, "name", "data");
                var fakeElement = { a: 42 };

                spyOn(vm, 'showElement').and.callThrough();

                vm.showElement(fakeElement);

                expect(vm.showElement).toHaveBeenCalledWith({ a: 42 });
            });
        });

    });

})();