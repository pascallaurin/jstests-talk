# Unit testing JavaScript

This repo is for my talk on JavaScript unit tests based on the following stack

* Node.js
* npm
* Gulp
* Jasime
* A dash of VS Code

You can find the slides for the presentation here (in French) on [SlideShare](http://fr.slideshare.net/PascalLaurin/tests-automatiss-java-script)

## Demo script for the presentation

### Node.js demo
- **node** (interactive mode)

### npm demo
- In a blank folder
    - **npm init** to create package.json file
    - **npm install** gulp --save-dev
- Open VS Code
    - Create **gulpfile.js**
            var gulp = require("gulp");
            gulp.task("default", function() { … });
    - run **gulp** in cmd
- Copy **var files** and **test task** from final code
    - Update default to use **["test"]**
- **npm install gulp-jasmine --save-dev**
- Copy **demo.tests.js** to **tests**
    - Comment **For code.js** section
    - Run **gulp test**
- Copy **demo.js** to **src**
    - Uncomment **For code.js** section
        - Keep **spyOn** and **expect** commented
    - Copy **globals.js** to **tests** and **jQuery.js** to **Shims**
    - Run **gulp test**
- Uncomment **spyOn** and **expect**
    - Run **gulp test**
    - Remove **.and.callThrough();**
    - Run **gulp test**
- Copy **cover** task from final code
    - Fix failing tests
    - Run **gulp cover**

### Visual Studio 2015 Demo
- Create empty **ASP.NET 5 WebApp**
- Copy
    - **package.json, gulpfile.js, tests** to **root**
    - **src** to **wwwroot**
- **npm install** to restore modules
- Update
    - test file for **wwwroot/src/demo.js**
    - gulpfile for **istanbul**
- gulp should work
- Open **Task Runner Explorer**
    - Run **test** and **coverage**
    - Bind **test** to **AfterBuild**
    - Build

### Node tools for Visual Studio
- File new project (JavaScript)
- npm node under **Dependencies**
    - Requires to be Node.js project for install
- Intellisense in **package.json**
- **Node Interactive Window**
    - **.npm** only in Node.js project
- Debugging with mocha only
- **Test explorer** with mocha and Node.js project only

### Debugging in VSCode
- Click the **Debug** icon than **Gear** icon
- Change **program** for **"./node_modules/gulp/bin/gulp.js"**
- Put a break point in VM test and click **Play** button